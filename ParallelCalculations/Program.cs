﻿/*Многопоточный проект
Цель:

Применение разных способов распараллеливания задач и оценка оптимального способа реализации.

Описание/Пошаговая инструкция выполнения домашнего задания:

    Напишите вычисление суммы элементов массива интов:

    Обычное
    Параллельное (для реализации использовать Thread, например List)
    Параллельное с помощью LINQ

    Замерьте время выполнения для 100 000, 1 000 000 и 10 000 000
    Укажите в таблице результаты замеров, указав:

    Окружение (характеристики компьютера и ОС)
    Время выполнения последовательного вычисления
    Время выполнения параллельного вычисления
    Время выполнения LINQ
    Пришлите в чат с преподавателем помимо ссылки на репозиторий номера своих строк в таблице.
*/

using System.Collections.Concurrent;
using System.Diagnostics;

// Не понял о каких номерах строк идет речь:
//      Пришлите в чат с преподавателем помимо ссылки на репозиторий номера своих строк в таблице.

namespace ParallelCalculations;

internal class Program
{
    static void Main()
    {
        int threadsCount = 8;
        int[] arraysLengths = [100_000, 1_000_000, 10_000_000, 100_000_000];
        List <int[]> arrays = new(arraysLengths.Length);
        List<string> headerRow = new(arraysLengths.Length + 1) { "Метод вычисления      " };
        foreach (int len in arraysLengths) 
        { 
            arrays.Add(CreateArray(len));
            headerRow.Add(len.ToString(format: "n") + new string(' ', 15));
        }
        List<List<string>> resultTable = new(5) { 
            headerRow, 
            new(arraysLengths.Length + 1) { "Последовательное      " },
            new(arraysLengths.Length + 1) { "Параллельное (Threads)" },
            new(arraysLengths.Length + 1) { "Параллельное (PLinq)  " },
            new(arraysLengths.Length + 1) { "Параллельное(Parallel)" }
        };
        Stopwatch sw = new();

        for (int i = 0; i < arrays.Count; i++)
        {
            sw.Restart();
            long sum = SumOneThreaded(arrays[i]);
            sw.Stop();
            resultTable[1].Add($"Результат: {sum}, Мс: {sw.ElapsedMilliseconds}");

            sw.Restart();
            sum = SumMultiThreaded(arrays[i], threadsCount);
            sw.Stop();
            resultTable[2].Add($"Результат: {sum}, Мс: {sw.ElapsedMilliseconds}");

            sw.Restart();
            sum = SumPLinq(arrays[i], threadsCount);
            sw.Stop();
            resultTable[3].Add($"Результат: {sum}, Мс: {sw.ElapsedMilliseconds}");

            // Это от себя добавил вычисление с помощью Parallel.Foreach из интереса
            sw.Restart();
            sum = SumParallelForeach(arrays[i], threadsCount);
            sw.Stop();
            resultTable[4].Add($"Результат: {sum}, Мс: {sw.ElapsedMilliseconds}");
        }

        foreach (List<string> row in resultTable)
        {
            Console.WriteLine(String.Join(" | ",row));
        }
    }

    static int[] CreateArray(int length)
    {
        int[] array = new int[length];
        Random rnd = new();
        for (int i = 0; i < length; i++)
        {
            array[i] = rnd.Next(1, 100);
        }
        return array;
    }

    static long SumOneThreaded(int[] array)
    {
        //несмотря на многословность, этот код работает быстрее на большом объеме данных
        //long sum = 0L;
        //for (int i = 0;i < array.Length;i++)
        //{
        //    sum += array[i];
        //}
        //return sum;

        return array.Sum(x => (long)x);
    }

    static long SumMultiThreaded(int[] array, int threadsCount)
    {
        int chunkSize = (int)Math.Ceiling((double)array.Length / threadsCount);
        IEnumerable<int[]> chunks = array.Chunk(chunkSize);
        List<Thread> threads = new();
        ConcurrentBag<long> microSums = new();

        foreach (int[] chunk in chunks)
        {
            Thread t = new(() => microSums.Add(SumOneThreaded(chunk)));
            t.Start();
            threads.Add(t);
        }

        foreach (Thread t in threads)
        {
            t.Join();
        }

        return microSums.Sum();
    }

    static long SumPLinq(int[] array, int threadsCount)
    {
        return array.AsParallel().WithDegreeOfParallelism(threadsCount).Sum(x => (long)x);
    }

    // добавил из любопытства посмотреть на Parallel.Foreach()
    static long SumParallelForeach(int[] array, int threadsCount)
    {
        int chunkSize = (int)Math.Ceiling((double)array.Length / threadsCount);
        IEnumerable<int[]> chunks = array.Chunk(chunkSize);
        ConcurrentBag<long> microSums = new();

        Parallel.ForEach(chunks,
            new ParallelOptions {  MaxDegreeOfParallelism = threadsCount },
            chunk => microSums.Add(SumOneThreaded(chunk)));

        return microSums.Sum();
    }
}
